package com.daniel.novoappcomputacaografica;

import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.opengl.GLSurfaceView;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

//Classe que implementa a interface do renderizador
class OpenglRender implements GLSurfaceView.Renderer, View.OnTouchListener, SensorEventListener {

    ArrayList<Geometria> gemetricos = new ArrayList<>();

    int largura = 0;
    int altura = 0;
    float angulo = 0;

    boolean chegou;

    boolean clicou;
    boolean soltou;

    float hora = 0;
    float minuto = 0;
    float segundo = 0;
    float milisegundo = 0;

    float posicaoX = 0f;
    float posicaoY = 0f;
    private Geometria objMove = null;
    GL10 openglT = null;

    private Quadrado relogio;

    //Variável para armazenar a direção em que o objeto está se movimentando no eixo X
    float direcaoX = 1f;

    //Variável para armazenar a direção em que o objeto está se movimentando no eixo Y
    float direcaoY = 1f;

    FloatBuffer buffer1;
    FloatBuffer buffer2;

    //Metodo obrigatorio definido pela interface Renderer (chamado quando a superficie e criada)
    public void onSurfaceCreated(GL10 opengl, EGLConfig config) {
        opengl.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    }

    //Metodo obrigatorio definido pela interface Renderer (chamado quando tamanho superficie alterado)
    public void onSurfaceChanged(GL10 opengl, int width, int height) {
        //Configura o tamanho da viewport (Toda Tela)
        opengl.glViewport(0, 0, width, height);

        largura = width;
        altura = height;

        //Habilita a matriz para a configuracao do tipo de projecao
        opengl.glMatrixMode(GL10.GL_PROJECTION);
        opengl.glLoadIdentity();
        opengl.glOrthof(0.0f, width, 0.0f, height, -1.0f, 1.0f);

        //Habilita a matriz de modelos e camera
        opengl.glMatrixMode(GL10.GL_MODELVIEW);
        opengl.glLoadIdentity();

        //Habilita o desenho por vertices
        opengl.glEnableClientState(GL10.GL_VERTEX_ARRAY);

        /*float[] vetCoords1 =
                {
                        0.0f, 0.0f,
                        0, altura/2,
                        largura/2 , 0,
                        largura/2, altura/2};//QUADRADO CERTO */

        //Aloca memoria e define as coordenadas da primitiva
//        float[] vetCoords1 =
//                {
//                        0, 0,
//                        0, altura/2,
//                        largura/2 , 0,
//                        largura/2, altura/2};//QUADRADO ERRADO

        //Aloca memoria e define as coordenadas da primitiva
        //Põe o contro do retângulo no ponto de ancoragem (0,0)
//        float[] vetCoords1 =
//                {
//                        -largura/4, -altura/4, //(x,y)
//                        -largura/4, altura/4, //(x,y)
//                        largura/4 , -altura/4, //(x,y)
//                        largura/4, altura/4   //(x,y)
//                };//QUADRADO ERRADO
//
//        //Cria o vetor de coordenadas
//        FloatBuffer buffer1 = generateBuffer(vetCoords1);
//
//        //Registra as coordenadas na maquina OpenGL
//        opengl.glVertexPointer(2,
//                GL10.GL_FLOAT,
//                0, buffer1);
    }

    FloatBuffer generateBuffer(float[] vetor) {
        ByteBuffer prBuffer = //aloca memoria em bytes para os vertices
                ByteBuffer.
                        allocateDirect(vetor.length
                                * 4);

        //Ordena os enderecos de memoria conforme
        // a arquitetura do  processador
        prBuffer.order(ByteOrder.nativeOrder());

        //Gera o encapsulador, limpa, insere o vetor java
        //Retira eventuais sobras de memoria, retorna
        FloatBuffer prFloat =
                prBuffer.asFloatBuffer();
        prFloat.clear();
        prFloat.put(vetor);
        prFloat.flip();

        return prFloat;

    }

    //Metodo obrigatorio definido pela interface Renderer (loop chamado a cada frame)
    public void onDrawFrame(GL10 opengl) {
        openglT = opengl;
        opengl.glClear(GL10.GL_COLOR_BUFFER_BIT);
        if (relogio==null){
            relogio = quadrado();
        }


//        if (chegou){
//            angulo -= 0.1f;
//            if (angulo <= -12){
//                chegou = false;
//            }
//        }else {
//            angulo += 0.1f;
//            if (angulo >= 12){
//                chegou = true;
//            }
//        }



        relogio.setCores(1, 1, 1);
        relogio.setPosicao(largura/2, altura/2);
        relogio.setRotacao(angulo, 1);
        relogio.setTempo(23, 40, 50);
        relogio.desenha();

//        List<Geometria> lista = new ArrayList<>(gemetricos);
//        for (Geometria g : lista) {
//            g.desenha();
//        }
//
        if (objMove != null) {
            objMove.setPosicao(posicaoX, posicaoY);
        }
//        Log.i("INFOX", posicaoX+"");
//        Log.i("INFOY", posicaoY+"");

    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
//        posicaoX = event.getX();
//        posicaoY = altura - event.getY();
//
//        if (event.getAction() == MotionEvent.ACTION_MOVE) {
//            soltou = true;
//            clicou = false;
//            posicaoX = event.getX();
//            posicaoY = altura - event.getY();
//        }
//
//        if (objMove == null) {
//            if (event.getAction() == MotionEvent.ACTION_UP) {
////                gemetricos.add(formaAletoria());
//                clicou = true;
//                soltou = false;
//            }
//        }
//
//        if (event.getAction() == MotionEvent.ACTION_DOWN) {
//            posicaoX = (int) event.getX();
//            posicaoY = altura - (int) event.getY();
//
//            objMove = new Quadrado();
//            objMove = this.getObjeto((int) posicaoX, (int) posicaoY);
//        }

        return true;
    }

    public Geometria getObjeto(int posX, int posY) {
        int pontoBase = 0;

        for (int i = 0; i < gemetricos.size(); i++) {
            if (posX > gemetricos.get(i).getPosicaoX() - 100 && posY > gemetricos.get(i).getPosicaoY() - 100) {
                return gemetricos.get(i);
            }

        }
        return null;
    }

    public Geometria formaAletoria() {
        Random r = new Random();
        Geometria g = null;
        int ri = r.nextInt(3);
        g = new Quadrado();
        g.setAllProperties(largura, altura, openglT, 0.75f);
        g.setPosicao(posicaoX, posicaoY);
        g.setRotacao(angulo, 1);
        g.setCores((float) Math.random(), (float) Math.random(), (float) Math.random());
        return g;
    }

    public Quadrado quadrado() {
        Random r = new Random();
        Quadrado g = new Quadrado();
        int ri = r.nextInt(3);
        g.setAllProperties(largura, altura, openglT, 0.75f);
        g.setPosicao(posicaoX, posicaoY);
        g.setRotacao(angulo, 1);
        g.setCores((float) Math.random(), (float) Math.random(), (float) Math.random());
        return g;
    }

    @Override
    public void onSensorChanged(SensorEvent event) {

//        Log.i("INFO", event.values[2]+"");
//
//        posicaoX = posicaoX + event.values[0];
//        posicaoY = posicaoY + event.values[1];
//        angulo = angulo + event.values[2];
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }
}
