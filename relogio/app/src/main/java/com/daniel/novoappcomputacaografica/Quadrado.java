package com.daniel.novoappcomputacaografica;

import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

public class Quadrado extends Geometria {
    float hora = 0;
    float minuto = 0;
    float segundo = 0;

    float milisegundo = 0;

    boolean iniciou;

    public Quadrado(float largura, float altura, GL10 gl10, float proporcao) {
        super(largura, altura, gl10, proporcao);
    }

    public Quadrado() {
        refX -= 50;
        refY += 200;
    }

    @Override
    public void desenha() {
        float aHora = ((hora * 6));
        float aMinuto = ((minuto * 6))-aHora;
        float aSegundo = ((segundo * 6))-aMinuto;

        if(milisegundo == 60){
            segundo++;
            if (segundo==60){
                minuto++;
                if (minuto==60){
                    hora++;
                    minuto = 0;
                }
                if (hora == 23){
                    hora = 0;
                }
                segundo = 0;
            }
            milisegundo=0;
        }else{
            milisegundo++;
        }


        float[] vetCoords1 =
                {
                        (-refX) * proporcao, (0) * proporcao,
                        (-refX) * proporcao, (refY * 2) * proporcao,
                        (refX) * proporcao, (0) * proporcao,
                        (refX) * proporcao, (refY * 2) * proporcao};//QUADRADO CERTO

        float[] vetCoordsMinuto =
                {
                        (-refX/2) * proporcao, (0) * proporcao,
                        (-refX/2) * proporcao, (refY * 2) * proporcao,
                        (refX/2) * proporcao, (0) * proporcao,
                        (refX/2) * proporcao, (refY * 2) * proporcao};//QUADRADO CERTO

        float[] vetCoordsSegundo =
                {
                        (-refX/3) * proporcao, (0) * proporcao,
                        (-refX/3) * proporcao, (refY * 2) * proporcao,
                        (refX/3) * proporcao, (0) * proporcao,
                        (refX/3) * proporcao, (refY * 2) * proporcao};//QUADRADO CERTO

//        float[] vetCoords1 =
//                {
//                        (-refX)*proporcao, (refY)*proporcao,
//                        (-refX)*proporcao, (-refY)*proporcao,
//                        (refX)*proporcao, (refY)*proporcao,
//                        (refX)*proporcao, (-refY)*proporcao};//QUADRADO CERTO

        //Cria o vetor de coordenadas
        FloatBuffer buffer1 = generateBuffer(vetCoords1);

        //Registra as coordenadas na maquina OpenGL
        gl10.glVertexPointer(2,
                GL10.GL_FLOAT,
                0, generateBuffer(vetCoords1));

        gl10.glLoadIdentity();

        gl10.glTranslatef(posicaoX, posicaoY, 0);
        gl10.glRotatef(-aHora, 0, 0, 1);
        //Transladar um objeto
        gl10.glColor4f(redColor, greenColor, blueColor, 1.0f);
        gl10.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);

//        anguloRotacao = anguloRotacao*1.5f;
        //Registra as coordenadas na maquina OpenGL
        gl10.glVertexPointer(2,
                GL10.GL_FLOAT,
                0, generateBuffer(vetCoordsMinuto));

        gl10.glPushMatrix();
        gl10.glColor4f(1, 0, 0, 1.0f);
//        gl10.glRotatef(anguloRotacao, 0,0,1);
        gl10.glTranslatef(0, 0, 0);
        gl10.glRotatef(-aMinuto, 0, 0, 1);
//        gl10.glScalef(0.5f, 0.5f, 0.5f);
        //Transladar um objeto
        gl10.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
//
//        anguloRotacao = anguloRotacao*2f;
        gl10.glVertexPointer(2,
                GL10.GL_FLOAT,
                0, generateBuffer(vetCoordsSegundo));

        gl10.glColor4f(0, 2, 0, 1.0f);
//        gl10.glRotatef(anguloRotacao, 0,0,1);
        gl10.glTranslatef(0, 0, 0);
        gl10.glRotatef(-aSegundo, 0, 0, 1);
//        gl10.glScalef(0.5f, 0.5f, 0.5f);
        //Transladar um objeto
        gl10.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
        gl10.glPopMatrix();

    }

    public void setTempo(float hora, float minuto, float segundo) {
        if (!iniciou){
            this.hora = hora;
            this.minuto = minuto;
            this.segundo = segundo;
            iniciou = true;
        }
    }
}
