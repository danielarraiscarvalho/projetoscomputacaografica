package com.daniel.texturas;

import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

public class Quadrado extends Geometria {

    @Override
    public void desenha() {

        float[] vetCoords1 =
                {
                        (-refX)*proporcao, (refY)*proporcao,
                        (-refX)*proporcao, (-refY)*proporcao,
                        (refX)*proporcao, (refY)*proporcao,
                        (refX)*proporcao, (-refY)*proporcao};//QUADRADO CERTO

        float[] vetCoordsTextura =
                {
                        (-refX)*proporcao, (refY)*proporcao,
                        (-refX)*proporcao, (-refY)*proporcao,
                        (refX)*proporcao, (refY)*proporcao,
                        (refX)*proporcao, (-refY)*proporcao};//QUADRADO CERTO
        //Cria o vetor de coordenadas
        FloatBuffer buffer1 = generateBuffer(vetCoords1);

        //Registra as coordenadas na maquina OpenGL
        gl10.glVertexPointer(2,
                GL10.GL_FLOAT,
                0, generateBuffer(vetCoords1));

        gl10.glLoadIdentity();

        gl10.glTranslatef(posicaoX, posicaoY, 0);
        gl10.glRotatef(anguloRotacao, 0, 0, 1);
        //Transladar um objeto
        gl10.glColor4f(redColor, greenColor, blueColor, 1.0f);
        gl10.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
    }
}
