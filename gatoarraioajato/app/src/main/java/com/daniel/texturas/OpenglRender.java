package com.daniel.texturas;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.opengl.GLSurfaceView;
import android.opengl.GLUtils;
import android.view.MotionEvent;
import android.view.View;

import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.FloatBuffer;
import java.util.ArrayList;

import javax.microedition.khronos.egl.EGLConfig;
import javax.microedition.khronos.opengles.GL10;

//Classe que implementa a interface do renderizador
class OpenglRender implements GLSurfaceView.Renderer, View.OnTouchListener, SensorEventListener {

    ArrayList<Geometria> gemetricos = new ArrayList<>();

    int largura = 0;
    int altura = 0;
    float angulo = 0;

    boolean chegou;
    boolean inverter;

    boolean clicou;
    boolean soltou;

    float hora = 0;
    float minuto = 0;
    float segundo = 0;
    float milisegundo = 0;

    float fig = 7;

    int codTextura = 0;

    float posicaoX = 0f;
    float posicaoY = 0f;
    private Geometria objMove = null;
    GL10 openglT = null;

    private Quadrado relogio;

    //Variável para armazenar a direção em que o objeto está se movimentando no eixo X
    float direcaoX = 1f;

    //Variável para armazenar a direção em que o objeto está se movimentando no eixo Y
    float direcaoY = 1f;

    FloatBuffer buffer1;
    FloatBuffer buffer2;

    Activity activity;

    //Metodo obrigatorio definido pela interface Renderer (chamado quando a superficie e criada)
    public void onSurfaceCreated(GL10 opengl, EGLConfig config) {
        opengl.glClearColor(0.0f, 0.0f, 1.0f, 1.0f);
    }

    //Metodo obrigatorio definido pela interface Renderer (chamado quando tamanho superficie alterado)
    public void onSurfaceChanged(GL10 opengl, int width, int height) {
        //Configura o tamanho da viewport (Toda Tela)
        opengl.glViewport(0, 0, width, height);

        largura = width;
        altura = height;

        //Habilita a matriz para a configuracao do tipo de projecao
        opengl.glMatrixMode(GL10.GL_PROJECTION);
        opengl.glLoadIdentity();
        opengl.glOrthof(0.0f, width, 0.0f, height, -1.0f, 1.0f);

        //Habilita a matriz de modelos e camera
        opengl.glMatrixMode(GL10.GL_MODELVIEW);
        opengl.glLoadIdentity();

        int refX = 300;
        int refY = 150;

        float[] vetCoords1 =
                {
                        (-refX), (refY),
                        (-refX), (-refY),
                        (refX), (refY),
                        (refX), (-refY)};//QUADRADO CERTO


        //Habilita o desenho por vertices
        opengl.glEnableClientState(GL10.GL_VERTEX_ARRAY);
        opengl.glEnable(GL10.GL_TEXTURE_2D);
        opengl.glEnableClientState(GL10.GL_TEXTURE_COORD_ARRAY);

        opengl.glEnable(GL10.GL_BLEND);
        opengl.glEnable(GL10.GL_ALPHA_TEST);
        opengl.glBlendFunc(GL10.GL_SRC_ALPHA, GL10.GL_ONE_MINUS_SRC_ALPHA);
        opengl.glEnableClientState(GL10.GL_VERTEX_ARRAY);

        //Registra as coordenadas na maquina OpenGL
        opengl.glVertexPointer(2, GL10.GL_FLOAT, 0, generateBuffer(vetCoords1));



        codTextura = carregaTextura(opengl, R.mipmap.sprites);
    }

    FloatBuffer generateBuffer(float[] vetor) {
        ByteBuffer prBuffer = //aloca memoria em bytes para os vertices
                ByteBuffer.
                        allocateDirect(vetor.length
                                * 4);

        //Ordena os enderecos de memoria conforme
        // a arquitetura do  processador
        prBuffer.order(ByteOrder.nativeOrder());

        //Gera o encapsulador, limpa, insere o vetor java
        //Retira eventuais sobras de memoria, retorna
        FloatBuffer prFloat =
                prBuffer.asFloatBuffer();
        prFloat.clear();
        prFloat.put(vetor);
        prFloat.flip();

        return prFloat;

    }

    public float[] vetor(float c, boolean inverter){
        int cc = (int) c;
        float x = c%2 == 0 ? (inverter ? 0.5f:0f) : (inverter ? 0f:0.5f);
        float x2 = x + 0.5f;
        float y = c == 0 ? (1f/4f) : (cc/2)*(1f/4f);
        if (inverter){
            x = x - (x+x);
            x2 = x2 - (x2+x2);
        }
        float[] v0 = {
                x, y,
                x, y-(1f/4f),
                x2, y,
                x2, y-(1f/4f)
        };

        return v0;
    }

    //Metodo obrigatorio definido pela interface Renderer (loop chamado a cada frame)
    public void onDrawFrame(GL10 opengl) {

        openglT = opengl;
        opengl.glClear(GL10.GL_COLOR_BUFFER_BIT);

        opengl.glLoadIdentity();

        opengl.glBindTexture(GL10.GL_TEXTURE_2D, 1);
        opengl.glTranslatef(largura/2, (posicaoY), 0);
        opengl.glRotatef(90, 0, 0, 1);
        opengl.glColor4f(1, 1, 1, 1.0f);
        opengl.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 4);
        //Registra as coordenadas na maquina OpenGL

        minuto ++;
        if (minuto == 8){
            if (posicaoY >= altura){
                chegou = true;
                inverter = true;
            }
            if (chegou){
                posicaoY = posicaoY - 100;
                if (posicaoY <= 0 ){
                    chegou = false;
                    inverter = false;
                }
            }else{
                posicaoY = posicaoY + 100;
            }

            opengl.glTexCoordPointer(2, GL10.GL_FLOAT, 0, generateBuffer(vetor(fig, chegou)));

            if (chegou){
                if (inverter){
                    fig = fig + 1;
                    if (fig <= -7 ){
                        inverter = false;
                    }
                }else if (!inverter){
                    fig = fig - 1;
                }
            }else {
                if (inverter){
                    fig = fig - 1;
                    if (fig <= 0 ){
                        inverter = false;
                    }
                }else if (!inverter){
                    fig = fig + 1;
                }
            }
            minuto = 0;
        }

    }

    public int carregaTextura(GL10 opengl, int codTextura){

        //carrega imagem na memoria
        Bitmap bitmap = BitmapFactory.decodeResource(activity.getResources(), codTextura);

        //define um array para guardar os ids de texturas
        int[] idTextura = new int[1];

        //gerar as áreas na gpu e cria um id pra cada um
        opengl.glGenTextures(1, idTextura, 0);

        //aponta a máquina opengl para uma das áreas de memória criadas na GPU
        opengl.glBindTexture(GL10.GL_TEXTURE_2D, idTextura[0]);

        //copiar a imagem da ram para a VRAM
        GLUtils.texImage2D(GL10.GL_TEXTURE_2D, 0, bitmap, 0);

        //aponta a VVRAM opengl  para o nada
        opengl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MIN_FILTER, GL10.GL_NEAREST);
        opengl.glTexParameterf(GL10.GL_TEXTURE_2D, GL10.GL_TEXTURE_MAG_FILTER, GL10.GL_NEAREST);

        //libera a memoria ram
        opengl.glBindTexture(GL10.GL_TEXTURE_2D, 0);

        bitmap.recycle();

        return idTextura[0];
    }

    public Geometria getObjeto(int posX, int posY) {
        int pontoBase = 0;

        for (int i = 0; i < gemetricos.size(); i++) {
            if (posX > gemetricos.get(i).getPosicaoX() - 100 && posY > gemetricos.get(i).getPosicaoY() - 100) {
                return gemetricos.get(i);
            }

        }
        return null;
    }


    @Override
    public void onSensorChanged(SensorEvent event) {
        
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    @Override
    public boolean onTouch(View v, MotionEvent event) {
        return false;
    }
}
