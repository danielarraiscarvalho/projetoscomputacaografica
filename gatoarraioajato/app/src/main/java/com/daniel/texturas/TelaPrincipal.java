package com.daniel.texturas;

import android.app.Activity;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.opengl.GLSurfaceView;
import android.os.Bundle;


/*****************************************************************************
 * Programa: Exemplo3
 * Descricao: Demonstra como utilizar OpenGL para desenhar primitivas em Android
 * Autor: Silvano Malfatti
 * Local: Palmas-TO
 *****************************************************************************/

//Pacote da aplicacao (identificador unico)

//Classe principal da aplicacao
public class TelaPrincipal extends Activity {

    //Atrivutos da classe
    private GLSurfaceView prDrawSurface = null;
    private OpenglRender prRenderer = null;

    private SensorManager mSensorManager;
    private Sensor mAcelerometro;

    //Metodo sobrescrito da classe Activity
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        prRenderer = new OpenglRender();
        prRenderer.setActivity(this);
        prDrawSurface = new GLSurfaceView(this);
        prDrawSurface.setRenderer(prRenderer);
        //Registra o OnTouchLitener na superficie
        prDrawSurface.setOnTouchListener(prRenderer);

        setContentView(prDrawSurface);

//        mSensorManager = (SensorManager) getSystemService(SENSOR_SERVICE);
//        mAcelerometro = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
    }

    @Override
    protected void onResume() {
        super.onResume();
//        mSensorManager.registerListener(prRenderer, mAcelerometro, SensorManager.SENSOR_DELAY_UI);
    }

    @Override
    protected void onPause() {
        super.onPause();
//        mSensorManager.unregisterListener(prRenderer);
    }
}
