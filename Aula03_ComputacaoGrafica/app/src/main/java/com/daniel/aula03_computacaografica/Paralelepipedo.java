package com.daniel.aula03_computacaografica;

import java.nio.FloatBuffer;

import javax.microedition.khronos.opengles.GL10;

public class Paralelepipedo extends Geometria {

    public Paralelepipedo(float largura, float altura, GL10 gl10, float proporcao) {
        super(largura, altura, gl10, proporcao);
    }

    public Paralelepipedo() {

    }


    @Override
    public void desenha() {

        float refXExtra = refX / 2;

        float[] vetCoords1 =
                {

                ((-refX * 3) - refXExtra)*proporcao, (refY)*proporcao,
                ((-refX) - refXExtra)*proporcao, (-refY)*proporcao,
                -refXExtra*proporcao, (refY)*proporcao,
                (refX)*proporcao, (-refY)*proporcao,
                ((refX) - refXExtra)*proporcao, (refY)*proporcao,
                ((refX * 3) - refXExtra)*proporcao, (-refY)*proporcao};

        //Cria o vetor de coordenadas
        FloatBuffer buffer1 = generateBuffer(vetCoords1);

        //Registra as coordenadas na maquina OpenGL
        gl10.glVertexPointer(2,
                GL10.GL_FLOAT,
                0, buffer1);

        gl10.glTranslatef(posicaoX, posicaoY, 0);
        gl10.glRotatef(anguloRotacao, 0, 0, anguloZ);
        //Transladar um objeto
        gl10.glColor4f(redColor, greenColor, blueColor, 1.0f);
        gl10.glDrawArrays(GL10.GL_TRIANGLE_STRIP, 0, 6);
        gl10.glLoadIdentity();
    }
}
